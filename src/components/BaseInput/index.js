//@flow
import React from 'react'
import FormText from '../FormText'
import styled from 'styled-components'

const WrapperInput = styled.div`
display: flex;
flex-direction: column;
&>*{
    margin-botton:7px;
}
`

const StyledInput = styled.input`
border: 1px solid;
border-color: ${props => props.invalid ? props.theme.color.red : props.theme.color.gray};
font-size:15px;
border-radius:5px;
padding: 5px;
`

export type BaseInputProps = {
    error?:String,
    name:String,
    type?:string
}

type Props = {
    header?: any
}& BaseInputProps //aqui se concatena el base inputprops con las variables de Props

const BaseInput = ({error,name,type,header}: Props) => {
    return ( 
    <WrapperInput>
    {header}
    <StyledInput name = {name} type = {type} id = {name}  invalid={!!error}/>
    {error && <FormText color="red">{error}</FormText>}
    </WrapperInput>
    )
}

BaseInput.defaultProps = { type : "text"}

export default BaseInput