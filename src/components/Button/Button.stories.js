// @flow
import React from "react"

import Button from "."

export default {
  title: "Button",
  component: Button,
  parameters:{
    info:{
      inline:true
    }
  }
}

export const defaultButton = () => <Button>Click aquí</Button>

export const mediumButton = () => <Button size="medium">Click aquí</Button>

export const underlineButton = () => (
  <Button size="medium" underline>
    Click aquí
  </Button>
)
