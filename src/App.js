// @flow
import React from "react"
import logo from "./logo.svg"
import styled from "styled-components"

const Wrapper = styled.div`
  text-align: center;
`

export type Props = {
  label?: string | any,
  to: string,
  children: any
}

// type PropSaludar = {
//   nombre: string, direccion: string, telefono: number
// }

// function saludar ({nombre, direccion, telefono: number }: PropSaludar, otro: any) {

// }

const Link = ({ label, to, children }: Props) => (
  <a className="App-link" href={to} target="_blank" rel="noopener noreferrer">
    {label || children}
  </a>
)

function App() {
  return (
    <Wrapper>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Link to="/login">Click aquí</Link>
      </header>
    </Wrapper>
  )
}

export default App
